import Vue from 'vue'
import Vuex from 'vuex'
import  * as actions from './actions'
// * as 引入文件全部方法    nuex中配置
import * as getters from './getters'
import * as mutations from './mutations'
import { state } from './state'
// 获取state文件里的对象，这里面不是方法，是属性
Vue.use(Vuex)
export const store = new Vuex.Store({
    // 分别应用
    state,
    getters,
    mutations,
    actions
})